# NOTE


## Task runner

I'm sorry but I have the boilerplate only for Gulp so I used it.


## CSS

 * Removed Vendor Prefixes because gulp-autoprefixer add the useful ones
 * Files refactored:
    * _button.scss
    * _colors.scss
    * _form.scss
    * _project-gallery.scss
    * _typography.scss
    * main.scss


## JS

 * Files on which I worked:
    * AJAX.js
    * Car.js
    * Gallery.js