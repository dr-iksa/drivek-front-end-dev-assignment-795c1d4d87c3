// node_modules
const autoprefixer = require('gulp-autoprefixer');
const babelify     = require('babelify');
const browserify   = require('browserify');
const gulp         = require('gulp');
const gulpsync     = require('gulp-sync')(gulp);
const sass         = require('gulp-sass');
const source       = require('vinyl-source-stream');
const sourcemaps   = require('gulp-sourcemaps');


// Paths
const src     = './src';
const build   = './build';
// CSS
const css_input   = src + '/css/**/*.scss';
const css_build   = build + '/css/';
// const css_maps    = css_build + '/maps';  // Don't work with path
// JS
const js_entry      = src + '/js/main.js';
const js_input      = src + '/js/*.js';
const js_libs       = src + '/js/libs/*.js';
const js_build      = build + '/js/';
const js_build_libs = js_build + '/libs/';


// Options
// CSS
const sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};
// JS
const browserifyOptions = { 
    entries: js_entry, 
    debug: true 
};
const transformPresetsOptions = { 
    presets: ["es2015"] 
};


// Default tasks
gulp.task('default', () =>
    console.log(
        '\n\r' + 'SCRIPTS' + '\n\r' +
        'npm run build'    + '\n\r' +
        'npm run dev'    + '\n\r' +
        'npm run postinstall'    + '\n\r' +
        'npm run test'    + '\n\r' +
        'npm run watch'  + '\n\r'
    )
);

gulp.task('build', gulpsync.async(['css_dev', 'js_dev', 'js_move_libs']));
gulp.task('watch', gulpsync.sync(['build', 'watch_builded']));
gulp.task('watch_builded', gulpsync.async(['css_watch', 'js_watch']));


// CSS
gulp.task('css_dev', () =>
    gulp.src(css_input)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        // .pipe(sourcemaps.write(css_maps))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer())
        .pipe(gulp.dest(css_build))
        .resume()
);
gulp.task('css_watch', () =>
    gulp.watch(css_input, ['css_dev'])
        .on('change', function(event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        })
);


// JS
gulp.task('js_dev', () =>
    browserify(browserifyOptions)
        .transform("babelify", transformPresetsOptions)
        .bundle()
        .pipe(source('main.js'))
        .pipe(gulp.dest(js_build))
);
gulp.task('js_move_libs', () =>
    gulp.src(js_libs)
        .pipe(gulp.dest(js_build_libs))
);
gulp.task('js_watch', () =>
    gulp.watch(js_input, ['js_dev'])
        .on('change', function(event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        })
);
