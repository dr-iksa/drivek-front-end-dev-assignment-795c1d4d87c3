class Car {
    constructor(attrs, tags) {
        this.attrs = attrs;
        this.tags = tags;
    }

    get getCarId() {
        return this.attrs.carId;
    }
    get getCarMake() {
        return this.attrs.make;
    }

    get getCarModel() {
        return this.attrs.model;
    }

    get getCarImg() {
        return this.attrs.img;
    }
}

export default Car;