import ajaxGet from "./AJAX";
import Car from './Car';

class Gallery {
    constructor(
        url = './data/cars.json', 
        maxElementsPerPage = 20, 
        currentPage = 1
    ) {
        this.url = url;
        this.maxElementsPerPage = maxElementsPerPage;
        this.currentPage = currentPage;

        this.isActiveClass = 'is-active';
        this.paginationSelector = '.gallery__paging__cmd:not(gallery__paging__cmd--symbol)';
        this.gallerySelector = '.gallery__boxes';
    }

    _createDom(carId, make, model, img) {
        return `
            <article class="gallery__box" itemscope itemtype="http://schema.org/Car">
                <img class="gallery__box__img" src="${img}" alt="${make} ${model} image" itemprop="image">
                <header class="gallery__box__heading" itemprop="name">
                    <h2 class="gallery__box__heading--main">${make}</h2>
                    <h3 class="gallery__box__heading--secondary">${model}</h3>
                </header>
                <p class="gallery__box__car-id" itemprop="identifier">Car ID: ${carId}</p>
            </article>
        `;
    }

    _createCars(response) {
        let firstCarToRender = (this.currentPage - 1) * this.maxElementsPerPage;
        let lastCarToRender = firstCarToRender + this.maxElementsPerPage;

        lastCarToRender = lastCarToRender < response.length 
            ? lastCarToRender 
            : response.length;

        let $gallery = $(this.gallerySelector);
        $gallery.html('');

        for (let i = firstCarToRender; i < lastCarToRender; i++) {
            let car = new Car(response[i].attrs, response[i].tags);
            let dom = this._createDom(car.getCarId, car.getCarMake, car.getCarModel, car.getCarImg);

            $gallery.append(dom);
        }
    }

    _initCars() {
        ajaxGet(this.url)
            .then(JSON.parse)
            .then((response) => this._createCars(response))
            .catch((error) => { throw error; });
    }

    init(pageToLoad = 1) {
        this.currentPage = pageToLoad;
        this._initCars();

        let $paginationNumber = $(this.paginationSelector);
        $paginationNumber
            .on('click', (event) => {
                let $pageClicked = $(event.target);
                this.currentPage = parseInt($pageClicked.text(), 10);

                $pageClicked
                    .addClass(this.isActiveClass)
                    .siblings()
                    .removeClass(this.isActiveClass)
                this._initCars();
            });
    }
}

export default Gallery;